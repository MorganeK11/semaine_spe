#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <stdlib.h>
#include "toucher_ennemi.hpp"
using namespace sf;

int toucher_ennemi_up(Sprite &sprite,Sprite &bateau) //mouvement vers le haut
{
    sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y-PAS);

    FloatRect boundingBox = sprite.getGlobalBounds();
    FloatRect otherBox = bateau.getGlobalBounds();

    if (boundingBox.intersects(otherBox)) // il se touche
    {
        sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y+10*PAS);// il recule
    }

    return sprite.getPosition().y;
}

int toucher_ennemi_left(Sprite &sprite,Sprite &bateau)//mouvement vers la gauche
{
    sprite.setPosition(sprite.getPosition().x-PAS, sprite.getPosition().y);

    FloatRect boundingBox = sprite.getGlobalBounds();
    FloatRect otherBox = bateau.getGlobalBounds();

    if (boundingBox.intersects(otherBox))// il se touche
    {
        sprite.setPosition(sprite.getPosition().x+10*PAS, sprite.getPosition().y);// il recule
    }

    return sprite.getPosition().x;

}

int toucher_ennemi_right(Sprite &sprite,Sprite &bateau)//mouvement vers la droite
{
    sprite.setPosition(sprite.getPosition().x+PAS, sprite.getPosition().y);

    FloatRect boundingBox = sprite.getGlobalBounds();
    FloatRect otherBox = bateau.getGlobalBounds();

    if (boundingBox.intersects(otherBox))// il se touche
    {
        sprite.setPosition(sprite.getPosition().x-10*PAS, sprite.getPosition().y);// il recule
    }

    return sprite.getPosition().x;

}

int toucher_ennemi_down(Sprite &sprite,Sprite &bateau)//mouvement vers le bas
{
    sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y+PAS);

    FloatRect boundingBox = sprite.getGlobalBounds();
    FloatRect otherBox = bateau.getGlobalBounds();

    if (boundingBox.intersects(otherBox))// il se touche
    {
        sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y-10*PAS);// il recule
    }

    return sprite.getPosition().y;

}
