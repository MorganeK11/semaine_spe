#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <stdlib.h>
#define PAS 5

using namespace sf;

int toucher_ennemi_up(Sprite &sprite,Sprite &bateau);
int toucher_ennemi_left(Sprite &sprite,Sprite &bateau);
int toucher_ennemi_right(Sprite &sprite,Sprite &bateau);
int toucher_ennemi_down(Sprite &sprite,Sprite &bateau);
