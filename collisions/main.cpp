#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <stdlib.h>
#define BATEAU "bateau.jpg" // sprite mechant
#include "toucher_ennemi.hpp" //fonction a mettre
using namespace sf;
int main()
{
    // Create the main window
    RenderWindow fenetre (sf::VideoMode(800, 600), "WAY ?(Where Are You?)");

    int x=0, y=0, nbateau=400, positionbateau=400;// coordonnée gentil puis mechant
    // Load a sprite to display
    Texture texture;
    if (!texture.loadFromFile("cb.bmp"))
        return EXIT_FAILURE;
    Sprite sprite(texture);// sprite gentil

    Texture imagebateau ;
    if (!imagebateau.loadFromFile(BATEAU))
        printf("PB de chargement de l'image %s !\n", BATEAU);
    Sprite bateau; //sprite mechant
    bateau.setTexture(imagebateau);
    // Start the game loop
    while (fenetre.isOpen())
    {
        bateau.setPosition(nbateau,positionbateau);//position mechant
        sprite.setPosition(x,y);//position gentil

        Event evenement;
        while (fenetre.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                fenetre.close();
                break;
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Up )
                {
                    y=toucher_ennemi_up(sprite, bateau);
                }
                else if (evenement.key.code == Keyboard::Left )
                {
                    x=toucher_ennemi_left(sprite, bateau);
                }
                else if (evenement.key.code == Keyboard::Right )
                {
                    x=toucher_ennemi_right(sprite, bateau);
                }
                else if (evenement.key.code == Keyboard::Down )
                {
                    y=toucher_ennemi_down(sprite, bateau);
                }
                break;
            }
        }
        fenetre.clear() ;
        fenetre.draw(bateau);//mechant
        fenetre.draw(sprite);//gentil
        fenetre.display();
    }

    return EXIT_SUCCESS;
}
