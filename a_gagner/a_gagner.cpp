#include "a_gagner.hpp"
#define TEMPS_FIN 90
#define PARA "para.jpg"

/*
    Clock clok ; //avant le while isopen
        Time temps = clok.getElapsedTime() ;
        int time = temps.asSeconds();
        affiche_temps(fenetre, time, point);// fenetre pour afficher/ time pour le timer/ point pour le score sprite pour la sortie

*/

void affiche_temps(RenderWindow &fenetre, int time, int point ) //sprite pour sortie
{
    Font font;
    if (!font.loadFromFile("arial_narrow_7.ttf"))// --> police d'�criture
        printf("Erreur");//ATTENTION : METTRE LA POLICE (NOMFICHIER .ttf) DANS LE DOSSIER DU PROGRAMME !!!

    Text timer, score, fin;

    score.setPosition(150,220);
    score.setFont(font);
    char sc[30];
    sprintf(sc,"Score : " "%i", point); //donne le score
    score.setString(sc);
    score.setColor(Color::Black);
    score.setCharacterSize(40);

    timer.setPosition(400,220);
    timer.setFont(font);// --> choix de la police � utiliser


    char c[30];
    sprintf(c,"Temps : " "%i /%d", TEMPS_FIN-time, TEMPS_FIN); // donne le timer
    timer.setString(c);//--> choix de la cha�ne de caract�res � afficher
    //PS : POSSIBILITE DE METTRE UNE VARIABLE A LA PLACE DU MESSAGE (ICI "HELLO WORLD") SI ON A FAIT UN SPRINTF AVANT.
    timer.setColor(Color::Black);
    timer.setCharacterSize(40);

    int x=400, y=100;
    RectangleShape rect(Vector2f(x,y));
    rect.setFillColor(Color::Black); // case pour la victoire
    rect.setPosition(200,50);

    RectangleShape fen(Vector2f(770,570));
    fen.setFillColor(Color::White);//fond de l'ecran blanc
    fen.setPosition(15,15);


    fin.setPosition(245,75);
    fin.setFont(font);
    fin.setString("Vous avez gagn�!");//message gagner
    fin.setColor(Color(150,50,255));
    fin.setCharacterSize(45);


    Texture paratexture;
    paratexture.loadFromFile(PARA);

    Sprite para;
    para.setTexture(paratexture);
    para.setPosition(380,350);

    fenetre.draw(fen);
    fenetre.draw(score);
    fenetre.draw(timer);
    fenetre.draw(rect);
    fenetre.draw(fin);
    fenetre.draw(para);

}


/*
    avoir le sprite
    donner la position dans le while is open
*/
void arrettimer(RenderWindow &fenetre, int time, Sprite sprite/*Sprite mur*/)
{
    /*FloatRect boundingBox = mur.getGlobalBounds();
    FloatRect otherBox = sprite.getGlobalBounds();

    if (boundingBox.intersects(otherBox) || time==0)//pour arreter le timer
    {
        sleep(seconds(3600));
    }*/

    //fenetre.draw(sprite);

}
