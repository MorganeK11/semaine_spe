#include <SFML/Graphics.hpp>
#include "a_gagner.hpp"
#define SPRIT "cb.bmp"

using namespace sf;
//afficher la fenetre  si gagner avec le score et le timer
int main()
{
    RenderWindow fenetre(VideoMode(800, 600), "WAY?");

    Texture texture;
    texture.loadFromFile(SPRIT);

    Sprite sprite;
    sprite.setTexture(texture);
    int x=61, y=61;

    Clock clok ;
    int point=25;

    while (fenetre.isOpen())
    {

    sprite.setPosition(x,y);
        Time temps = clok.getElapsedTime() ; //a mettre dans le main
        int time = temps.asSeconds();

        fenetre.clear();
        affiche_temps(fenetre, time, point);
        arrettimer(fenetre,time, sprite);

        Event evenement;

        while (fenetre.pollEvent(evenement))
        {
            if (evenement.type == Event::Closed)
                fenetre.close();
            if (evenement.type == Event::KeyPressed)
                x-=2;
        }
        fenetre.display();
    }

    return EXIT_SUCCESS;
}
