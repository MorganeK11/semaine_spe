#include <SFML/Graphics.hpp>
#include <math.h>
#define PIXEL 30
#define EXTENSION 40

using namespace sf;

typedef struct
{
    int x, y;
}Point;

//fonctions calculs
int alea(int mini, int maxi);
void matrice_laby(int matrice[][100], int tailleX, int tailleY);
void numerotation_rec(int matrice[][100], int x, int y);
Point case_spawn(int matrice[][100], int tailleX, int tailleY);

//fonctions affichages
void affichage_laby(RenderWindow *fenetre, int matrice[][100], int tailleX, int tailleY);
void affichage_perso(RenderWindow *fenetre, Point position);
