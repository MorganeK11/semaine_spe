#include <SFML/Graphics.hpp>
#include <math.h>
#define PIXEL 30
#define EXTENSION 33

using namespace sf;

typedef struct
{
    int x, y;
}Point;


//fonction al�atoire utilis�e
int alea(int mini, int maxi)
{
    return rand()%(maxi-mini+1)+mini;
}

//case possible pour spawnage personnage
void numerotation_rec(int matrice[][100], int x, int y)
{
    //attribution case actuelle = -1 --> on peut s'y d�placer
    matrice[x][y]=-1;
    //condition reccurence direction Nord (deplacement --> x reste le meme, y=y-1)
    if(matrice[x][y-1]==0)
    {
        //dans les if, on fait appel � la fonction meme, pour num�rot� la case spawnable voisine
        numerotation_rec(matrice, x, y-1);
    }
    //condition r�ccurence direction Est (deplacement --> y reste le meme, x=x+1)
    if(matrice[x+1][y]==0)
    {
        numerotation_rec(matrice, x+1, y);
    }
    //condition r�ccurence direction Sud (deplacement --> x reste le meme, y=y+1)
    if(matrice[x][y+1]==0)
    {
        numerotation_rec(matrice, x, y+1);
    }
    //condition r�ccurence direction Ouest (deplacement --> y reste le meme, x=x-1)
    if(matrice[x-1][y]==0)
    {
        numerotation_rec(matrice, x-1, y);
    }
}

//choix de la case ou le personnage et les ennemis vont apparaitre
Point case_spawn(int matrice[][100], int tailleX, int tailleY)
{
    Point position_depart;
    do
    {
        position_depart.x=alea(0, tailleX-1);
        position_depart.y=alea(0, tailleY-1);
    }
    while(matrice[position_depart.x][position_depart.y]!=-1);
    return position_depart;
}

//creation matrice (sans affichage)
void matrice_laby(int matrice[][100], int tailleX, int tailleY)
{
    int x, y, x_atteignable, y_atteignable;

    //tableau a double entr�e ligne
    for(x=0; x<tailleX; x++)
    {
        //tableau a double entr�e colonne
        for(y=0; y<tailleY; y++)
        {

            //cr�ation de murs tout autour du labyrinthe
            if (x==0 || x==tailleX-1 || y==0 || y==tailleY-1)
            {
                matrice[x][y]=1;
            }

            //creation des pilliers sur les cases paires
            if (x%2==0 && y%2==0)
            {
                matrice[x][y]=1;

                //cr�ation des murs entre les pilliers
                //on a EXTENSION% de chance d'avoir une extension
                int extension=alea(1,100);
                if(extension<=EXTENSION)
                {
                    //direction et nbr de case de l'extension
                    int direction=alea(1,2);
                    int nbr_murs=alea(1,2);

                    //direction horizontale
                    if(direction==1)
                    {
                        //extension de 1
                        if(nbr_murs==1)
                        {
                            //extension � droite ou � gauche selon si -1^1=-1 (gauche) ou -1^2=1 (droite)
                            matrice[x+(int)pow(-1, alea(1,2))][y] = 1;
                        }
                        //extension de 2
                        else
                        {
                            //voisin de gauche
                            matrice[x-1][y]=1;
                            //voisin de droite
                            matrice[x+1][y]=1;
                        }
                    }
                    //direction verticale
                    else
                    {
                        //extension de 1
                        if(nbr_murs==1)
                        {
                            //extension � droite ou � gauche selon si -1^2=-1 (gauche) ou =1 (droite)
                            matrice[x][y+(int)pow(-1, alea(1,2))] = 1;
                        }
                        //extension de 2
                        else
                        {
                            //voisin de gauche
                            matrice[x][y-1]=1;
                            //voisin de droite
                            matrice[x][y+1]=1;
                        }
                    }
                }
            }
        }
    }

    //Choix du mur ou la sortie sera positionn�e
    int sortie_mur=alea(1,2);

    // Direction horizontal
    if(sortie_mur==1)
    {
        //choisit un x sur le haut ou le bas (pour la sortie)
        x=alea(0,1)*(tailleX-1);
        //choisit n'importe quel y qui n'est pas un pilier (pour la sortie)
        y=2*alea(0,(tailleY-1)/2)+1;

        //si on est sur mur horizontal (Aide au spawn du personnage par rapport a la position de la sortie)
        //Sur le mur du haut
        if (x==0)
        {
            // Prochaine case atteignable est la case du dessous (x+1)
            x_atteignable = 1;
        }
        //Sur le mur du bas
        else
        {
            // Prochaine case atteignable est la case du dessous (x-1)
            x_atteignable = tailleX-2;
        }
        //y reste le meme dans tous les cas
        y_atteignable = y;
    }

    //direction verticale
    else
    {
        //choisit un y sur la gauche ou sur la droite (pour la sortie)
        y=alea(0,1)*(tailleY-1);
        //choisit n'importe quel x qui n'est pas un pilier (pour la sortie)
        x=2*alea(0,(tailleX-1)/2)+1;

        //si on est sur mur vertical (Aide au spawn du personnage par rapport a la position de la sortie)
        //Sur le mur de gauche
        if (y==0)
        {
            // Prochaine case atteignable est la case de droite (y+1)
            y_atteignable = 1;
        }
        else
        {
            // Prochaine case atteignable est la case de gauche (y-1)
            y_atteignable = tailleY-2;
        }
        //x reste le meme dans tous les cas
        x_atteignable = x;
    }
    // Mise en place de la sortie
    matrice[x][y]=2;

    //num�rotation de toutes les cases spawnables
    numerotation_rec(matrice, x_atteignable, y_atteignable);

}

//affichage de chaque case de la matrice (passer en param�tre sous le nom de type_de_bloc
//qui repr�sente chaque valeur individuelle de la matrice)
void afficher_case(RenderWindow *fenetre, int x, int y, int type_de_bloc)
{
    //charge bloc brique mur
    Texture mur_brique;
    if (!mur_brique.loadFromFile("bloc_decouper.jpg"))
        printf("Probleme de chargement de l'image %s !\n", "bloc_decouper.jpg");

    //charge bloc sortie
    Texture mur_sortie;
    if (!mur_sortie.loadFromFile("bloc_sortie.jpg"))
        printf("Probleme de chargement de l'image %s !\n", "bloc_sortie.jpg");


    Texture spawn;
    if (!spawn.loadFromFile("spawnable.jpg"))
        printf("Probleme de chargement de l'image %s !\n", "spawnable.jpg");

    Sprite bloc;
    bloc.setPosition(x*PIXEL, (y+2)*PIXEL);

    if(type_de_bloc==1)
    {
        bloc.setTexture(mur_brique);
    }

    else if(type_de_bloc==2)
    {
        bloc.setTexture(mur_sortie);
    }
    //Remplissage des case non spawnables par des briques
    else if (type_de_bloc==0)
    {
        bloc.setTexture(mur_brique);
    }
    fenetre->draw(bloc);
}

//deplacement ennemi
Point deplacement_ennemi(int matrice[][100], Point position)
{
    Point p_dep;
    if (matrice[position.x-1][position.y]==-1)
        p_dep.x = position.x-1;
    // faire les if
    return p_dep
}

//affiche tout le labyrinthe
void affichage_laby(RenderWindow *fenetre, int matrice[][100], int tailleX, int tailleY)
{
    int x, y;
    for(x=0; x<=tailleX; x++)
    {
        for(y=0; y<=tailleY; y++)
        {
            //appel de la fonction qui affiche cases une par une
            afficher_case(fenetre, x, y, matrice[x][y]);
        }
    }
}

void affichage_perso(RenderWindow *fenetre, Point position)
{
    Texture spawnPerso;
    if (!spawnPerso.loadFromFile("spawn-perso.jpg"))
    printf("Probleme de chargement de l'image %s !\n", "spawn-perso.jpg");

    Sprite spawn_perso;
    spawn_perso.setPosition(position.x*PIXEL, (position.y+2)*PIXEL);
    spawn_perso.setTexture(spawnPerso);
    fenetre->draw(spawn_perso);
}

